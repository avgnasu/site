$(function() {
    $('#make_selects').change(function() {
        $.ajax({
            url: "/admin/filter",
            data: {
                make_id: $('#make_selects').val()
            },
            dataType: "script"
        });
    });

    $('#make_select').change(function() {
        $.ajax({
            url: "/admin/update_list",
            data: {
                make_id: $('#make_select').val()
            },
            dataType: "script",
            type: "POST"
        });
    });
})

