Simplesite.Breadcrumb =
   set:(attrs,level)->
	    $('#breadcrumb #make').html "<a href= #make/#{attrs.make_name}>#{attrs.make_name}</a>" if attrs.make_name? and level >= 1
	    $('#breadcrumb #model').html "<a href= #make/#{attrs.make_name}/#{attrs.model_name}>#{attrs.model_name}</a>" if attrs.model_name? and level >= 2
	    $('#breadcrumb #car').html "<a href= #make/#{attrs.make_name}/#{attrs.model_name}/#{attrs.car_id}>#{attrs.car_id}</a>" if attrs.car_id? and level >= 3
   clear:()->
   	$(".list").html ""