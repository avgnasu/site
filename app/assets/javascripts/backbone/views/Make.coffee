class Simplesite.Views.Make extends Backbone.View
  tagName: 'li'
  template : Simplesite.Templates.Make
  initialize: ()->

  events :
    'click':'active'

  render: ()->
    this.$el.html this.template(this.model.toJSON())
    console.log this
    return this;

  active: ()->
    console.log("model "+this.model.get('id')+" active")
    $('#nav li').removeClass('active')
    this.$el.addClass('active')