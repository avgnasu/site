Simplesite.Views.Models = Backbone.View.extend({
    initialize: function() {
        this.render();
    },
    render: function() {
        this.collection.each(function(model){
            var ModelView = new Simplesite.Views.Model({model: model});
            model.set({make_name:Simplesite.path.make_name})
            this.$el.append(ModelView.render().el);
        }, this);
        return this;
    }
});