class Simplesite.Views.Makes extends Backbone.View
    initialize: ()->
        this.render()

    render: ()->
        this.$el.html ''
        this.collection.each (make)->
            ModelView = new Simplesite.Views.Make model: make
            this.$el.append ModelView.render().el
        , this
        return this