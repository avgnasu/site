Simplesite.Views.Model = Backbone.View.extend({
    tagName: 'li',
    template : Simplesite.Templates.Model,
    initialize: function() {
    },
    events : {

    },
    render: function() {
        this.$el.html( this.template(this.model.toJSON()));
        return this;
    }
});