class Simplesite.Views.CarDetails extends Backbone.View
   initialize : ()->
       this.render()
    render : ()->
      Simplesite.helper.clearContent()
      source   = $("#entry-template").html();
      template = Handlebars.compile(source);
      context  = this.model.toJSON()
      html     = template(context);
      this.$el.html html
      return @