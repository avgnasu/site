class Simplesite.Views.Cars extends  Backbone.View
   initialize: ()->
       @render()
    render : ()->
        this.collection.each (car)->
            carView = new Simplesite.Views.Car (model: car)
            car.set make_name: Simplesite.path.make_name
            car.set model_name: Simplesite.path.model_name
            this.$el.append(carView.render().el)
        , this
        console.log this.el
        return this
