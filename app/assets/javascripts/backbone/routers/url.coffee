class Simplesite.Url extends Backbone.Router
  routes:
    '':'pager'
    'make/:name': 'pager'
    'make/:name/:model_name': 'pager'
    'make/:name/:model_name/:car_id' : 'pager'
    '*other' : 'other'

  
  getMakes: ()->
    if not Simplesite.Existing.Makes?
      Simplesite.Existing.Makes = new Simplesite.Collections.Make()
      Simplesite.Existing.Makes.fetch
        success :(msg)->
          new Simplesite.Views.Makes({collection:msg,el:'#nav'})
          Simplesite.attrs.thes.searchMakes()
    else
      Simplesite.attrs.thes.searchMakes();     

  getModels: ()->
      Simplesite.Existing.Models = new Simplesite.Collections.Model()
      Simplesite.Existing.Models.fetch
           success: (msg)->
            if not Simplesite.attrs.car_id? and not Simplesite.attrs.model_name?
              new Simplesite.Views.Models({collection: msg,el:'#list_models'}) 
            Simplesite.attrs.thes.searchModels()

  getCars: ()->
      Simplesite.Existing.Cars = new Simplesite.Collections.Car()
      Simplesite.Existing.Cars.fetch
          success: (msg)->
            if not Simplesite.attrs.car_id?
              new Simplesite.Views.Cars({collection: msg,el:"#content_body"})
            Simplesite.attrs.thes.searchCars()
  getCar: ()->
    new Simplesite.Views.CarDetails({model:Simplesite.Existing.Car,el:"#content_body"})



  searchMakes: ()->
    make_name = Simplesite.attrs.make_name
    if make_name? 
      console.log "валидируем марки",Simplesite.Existing.Makes
      if Simplesite.Existing.Makes.findWhere({slug:make_name})?
        Simplesite.Breadcrumb.set(Simplesite.attrs,1)
        Simplesite.path.set(Simplesite.attrs)
        Simplesite.vent.trigger('Have:Makes')
      else
        Simplesite.vent.trigger('error',"Нет марки с именем #{make_name}")
          

  searchModels: ()->
    console.log "валидируем модели",Simplesite.Existing.Models
    model_name = Simplesite.attrs.model_name
    if model_name?
      if Simplesite.Existing.Models.findWhere({slug:model_name})?
         Simplesite.Breadcrumb.set(Simplesite.attrs,2)
         Simplesite.vent.trigger('Have:Models')
      else
         Simplesite.vent.trigger('error',"Нет модели с именем  #{model_name}")

  searchCars: ()->
    console.log "валидируем машины",Simplesite.Existing.Cars
    car_id = Simplesite.attrs.car_id
    if car_id?
      Simplesite.Existing.Car = Simplesite.Existing.Cars.get(car_id)
      if Simplesite.Existing.Car?
         Simplesite.Breadcrumb.set(Simplesite.attrs,3)
         Simplesite.vent.trigger('Have:Cars')
      else
         Simplesite.vent.trigger('error',"Нет машины с id  #{car_id}")




  

  waitingMakes: ()->
    console.log "ожидаем марки"    
    Simplesite.vent.once('Have:Makes',@getModels,@)

  waitingModels: ()->
    console.log "ожидаем модели"    
    Simplesite.vent.once('Have:Models',@getCars,@)

  waitingCars: ()->
    console.log "ожидаем машины"
    Simplesite.vent.once('Have:Cars',@getCar,@)



  pager: (make_name,model_name,car_id)->
    Simplesite.helper.clearVent()
    Simplesite.helper.clearContent()
    Simplesite.Breadcrumb.clear()

    if make_name?
      @waitingMakes()
    if model_name?
      @waitingModels()
    if car_id?
      @waitingCars()

    Simplesite.attrs.set(make_name,model_name,car_id,@) 
    @getMakes()




  error: (msg)->
    console.log msg
    Simplesite.Existing.Error = new Simplesite.Views.Error
            model:{massage: msg}
            el:"#error"

  other: (msg)->
    alert "#{msg} - jump to the given address can not be"










  


