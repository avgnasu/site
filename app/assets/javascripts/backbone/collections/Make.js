Simplesite.Collections.Make = Backbone.Collection.extend({
    model:Simplesite.Models.Make,
    url: function(){
        return "/front/makes";
    }
});