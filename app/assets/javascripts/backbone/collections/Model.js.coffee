class Simplesite.Collections.Model extends Backbone.Collection
    initialize: ()->
    model : Simplesite.Models.Model
    url: ()->
      return "front/makes/#{Simplesite.path.make_name}/models"