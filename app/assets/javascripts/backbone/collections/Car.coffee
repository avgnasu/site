class Simplesite.Collections.Car extends Backbone.Collection
    model:Simplesite.Models.Car
    initialize: ()->
    url: ()->
      return "front/makes/#{Simplesite.path.make_name}/models/#{Simplesite.path.model_name}/cars"