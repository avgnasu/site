#= require_self
#= require_tree ./templates
#= require_tree ./models
#= require_tree ./views
#= require ./breadcrumb
#= require_tree ./routers

window.Simplesite = 
    Models: {}
    Collections: {}
    Routers: {}
    Views: {}
    Templates: {}
    Existing : {}

Simplesite.vent = _.extend({}, Backbone.Events);



Simplesite.helper =
  clearContent: ()->
   $('#list_models').html('')
   $('#content_body').html('')
   if Simplesite.Existing.Error?
    Simplesite.Existing.Error.remove();
  clearVent: ()->
    Simplesite.vent.off('Have:Cars') 
    Simplesite.vent.off('Have:Models')
    Simplesite.vent.off('Have:Makes')


Simplesite.path =
  make_name: null
  model_name: null
  car_id : null
  set: (attrs)->
      @make_name = attrs.make_name
      @model_name = attrs.model_name
      @car_id = attrs.car_id

Simplesite.attrs = 
  make_name: null
  model_name: null
  car_id : null
  thes : null
  set: (make_name,model_name,car_id,thes)->
    @make_name = make_name 
    @model_name = model_name 
    @car_id = car_id
    @thes = thes


$ ->
    Simplesite.Url = new Simplesite.Url;
    Simplesite.vent.on('error',Simplesite.Url.error);
    Backbone.history.start();