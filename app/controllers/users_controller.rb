class UsersController < ApplicationController
  skip_before_filter :signed_in
  def new
    if signed_in?
      flash[:info] = 'You are logined' # Not quite right!
      redirect_to :back
    else
      @user = User.new
    end
  end
  def create
    @user = User.new(params[:user])
    if @user.save
      flash[:success] = "Welcome to the Sample App!"
      sign_in @user
        redirect_to cars_path
    else
      render 'new'
    end
  end

end
