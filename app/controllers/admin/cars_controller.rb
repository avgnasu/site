class Admin::CarsController < Admin::AdminController

  before_filter   :have_a_car, only: [:edit,:show,:update,:destroy]
  respond_to :js, :json, :html

  def index
    @cars = Car.includes(:model).includes(:make).all
    nil_cookies
    select_make_nil
  end

  def new
    @car = Car.new
  end



  def create
    @car = Car.new(params[:car])
      if @car.save
        flash[:success] = "Create car success"
        redirect_to admin_cars_path
      else
        render 'new'
      end
  end

  def show
  end

  def edit
  end

  def update
     if @car.update_attributes(params[:car])
        redirect_to admin_car_path(@car)
     else
      render 'edit'
    end
  end

  def destroy
    @car.destroy
    flash[:info] = "Car destroyed."
    redirect_to admin_cars_path
  end



  def update_list
    unless params[:make_id].blank?
      make = Make.find(params[:make_id])
      @models = make.models.map{|a| [a.model_name, a.id] }
    else
      @models = []
    end

  end


  def filter
     @car = Car.select_car(cookies[:price_min],cookies[:price_max])

    unless params[:make_id].blank?
      @car = @car.where("make_id = ?",params[:make_id])
      cookies[:select_make] = params[:make_id]
    else
      select_make_nil
    end
     respond_with @car
  end


  def search_by_price
    cookies[:price_min] = params[:price_min]
    cookies[:price_max] = params[:price_max]

    @car = Car.select_car(cookies[:price_min],cookies[:price_max])

    temp_storing = cookies[:select_make]
    unless temp_storing.blank?
      @car = @car.where("make_id = ?",temp_storing)
    end
    respond_with @car
  end

  def delete_asset
    ad = Car.find(params[:car_id])
    asset = ad.attached_assets.find(params[:id])
    asset.asset.clear
    asset.destroy
    ad.save
    redirect_to :back
  end
  private


   def have_a_car
    unless @car = Car.where(id: params[:id]).first
     not_found
    end
   end
end
