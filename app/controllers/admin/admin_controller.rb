class Admin::AdminController < ApplicationController
  include SessionsHelper
  include Admin::CarsHelper

  before_filter   :signed_in

  def give_last_page
    session[:last_page] = request.env['HTTP_REFERER']
  end

  def not_found
    render :file => 'public/404.html', :status => :not_found, :layout => false
  end

  def signed_in
    if !signed_in?
      flash[:info] = 'You must be identified'
      redirect_to new_session_path
    end
  end
end