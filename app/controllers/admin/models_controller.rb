class Admin::ModelsController < Admin::AdminController

  before_filter   :have_a_model, only: [:update,:destroy,:show,:edit]

  def index
    @models = Model.includes(:make).all
  end

  def new
    @model = Model.new

  end

  def create
    @model = Model.new(params[:model])
    if @model.save
      flash[:success] = "Create model success"
      redirect_to admin_models_path
    else
      render 'new'
    end
  end

  def show
  end

  def edit
  end

  def update
    if @model.update_attributes(params[:model])
       @model.slug = nil
       @model.save 
       redirect_to admin_model_path(@model)
    else
      render 'edit'
    end
  end



  def destroy
    @model.destroy
    flash[:info] = "Model destroyed."
    redirect_to admin_models_path
  end

  private
  def  have_a_model
    unless @model = Model.friendly.find(params[:id])
      not_found
    end
  end

end
