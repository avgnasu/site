class Admin::MakesController < Admin::AdminController

  before_filter   :have_a_make, only: [:edit,:show,:update,:destroy]

    def index
      @makes = Make.all
    end

    def new
      @make = Make.new
    end

    def create
      @make = Make.new(params[:make])
      if @make.save
        flash[:success] = "Create make success"
        redirect_to admin_makes_path
      else
        render 'new'
      end
    end

    def show
    end

    def edit
    end

    def update
      if @make.update_attributes(params[:make])
         @make.slug = nil
         @make.save
        redirect_to admin_make_path(@make)
      else
        render 'edit'
      end
    end

    def destroy
      @make.destroy
      flash[:info] = @make.name+" destroyed."
      redirect_to admin_makes_path
    end

    private

    def have_a_make
      unless @make = Make.friendly.find(params[:id])
        not_found
      end
    end
end
