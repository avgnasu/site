class SessionsController < ApplicationController
  skip_before_filter :signed_in
  def index
    not_found
  end
  def new
    if signed_in?
      flash[:info] = 'You are logined' # Not quite right!
      redirect_to :back
    end
  end

  def create
    user = User.find_by_email(session_param[:email].downcase)
    if user && user.password ==  session_param[:password]
      sign_in user
      redirect_to cars_path
    else
      flash.now[:error] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
    sign_out
    redirect_to root_url
  end

  private
  def session_param
    params.require(:session).permit(:email,:password)
  end

end
