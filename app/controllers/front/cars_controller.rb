class Front::CarsController < Front::FrontController
  before_filter :have_a_cars
  def index
    render json: @cars.to_json(:methods => [:avatar_url])
  end
  def show
     render json: @cars.find(params[:id]).to_json(:methods => [:avatar_url])
  end
  def have_a_cars
   @cars = Make.friendly.find(params[:make_id]).models.friendly.find(params[:model_id]).cars
  end
end
