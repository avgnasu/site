class Front::ModelsController < Front::FrontController
  before_filter :have_a_model
  def index
    render json: @models
  end
  def show
    render json: @models.friendly.find(params[:id])
  end

  def have_a_model
    @models = Make.friendly.find(params[:make_id]).models
  end
end
