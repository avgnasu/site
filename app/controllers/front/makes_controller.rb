class Front::MakesController < Front::FrontController
  def index
    render json: Make.all
  end
  def show
    render json: Make.friendly.find(params[:id])
  end
end
