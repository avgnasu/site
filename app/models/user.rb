

class User < ActiveRecord::Base
  before_save :create_remember_token
  attr_accessible :name,:email,:password
  validates :name, presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX },
            uniqueness: { case_sensitive: false }
  validates :password, presence: true, length: { minimum: 6 }

  before_save { |user| user.email = email.downcase }

  private
  def create_remember_token
    self.remember_token = SecureRandom.urlsafe_base64
  end
end
