class Car < ActiveRecord::Base

  belongs_to :model
  belongs_to :make
  attr_accessible :displacement,:attached_assets_attributes,:fuel_type,:year,:model_id,:price,:features,:make_id,:avatar
  validates_presence_of :model_id, :price, :fuel_type, :displacement, :features, :make_id, :year
  validates_numericality_of :price, only_integer: true, greater_than: 0, allow_blank: true

  has_many :attached_assets, :as => :attachable
  accepts_nested_attributes_for :attached_assets, :allow_destroy => true

  default_scope order: 'cars.created_at DESC'


  def self.select_car(price_min,price_max)
    if  price_min||price_max
      Car.where("price > ?",price_min.to_i).where("price < ?",price_max)
    else
      Car.all
    end
  end

  def avatar_url
    urls = []
    self.attached_assets.each do |a|
       urls.push(a.asset.url(:normal))
    end
    urls
  end

end
