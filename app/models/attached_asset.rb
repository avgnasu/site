class AttachedAsset < ActiveRecord::Base
  belongs_to :car, :polymorphic => true
  has_attached_file :asset, :styles => { :mini => "64x32#", :normal => "640x320#"}
  attr_accessible :asset, :asset_file_name
end
