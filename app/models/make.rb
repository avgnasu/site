class Make < ActiveRecord::Base

  extend FriendlyId
  friendly_id :name, use: :slugged
  attr_accessible :name,:avatar

  has_many :models, dependent: :destroy
  has_many :cars, dependent: :destroy

  validates :name, presence: true
  validates_uniqueness_of  :name

  default_scope order: 'makes.created_at DESC'

  mount_uploader :avatar, AvatarUploader
end
