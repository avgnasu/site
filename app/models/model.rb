class Model < ActiveRecord::Base

  extend FriendlyId
  friendly_id :model_name, use: :slugged
  attr_accessible :model_name,:make_id,:avatar

  has_many :cars, dependent: :destroy
  belongs_to :make

  validates_presence_of  :model_name, :make_id
  validates_uniqueness_of  :model_name

  mount_uploader :avatar, AvatarUploader

  default_scope order: 'models.created_at DESC'

end
