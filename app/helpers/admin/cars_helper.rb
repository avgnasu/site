module Admin::CarsHelper
  def nil_cookies
    cookies[:price_min] = nil
    cookies[:price_max] = nil
  end
  def select_make_nil
    cookies[:select_make] = nil
    cookies[:select_model] = nil
  end
end
