 3.times do  |i|
   puts "make creating"
    make = Make.create(name: "Make#{i}")
    3.times do  |p|
      puts "model creating"
      model = make.models.create(model_name: "model#{p}_for_make#{i}")
      3.times do |j|
        puts "car creating"
        car = model.cars.create(fuel_type: "diesel", displacement: 4.5, year: 2013-rand(10),
                           price: 1000000-rand(500000), features: "AT#{j}",
                           make_id: make.id)
        car.attached_assets << AttachedAsset.create()
        car.attached_assets << AttachedAsset.create()
        car.attached_assets << AttachedAsset.create()
      end
    end
   end

User.create(name: "Admin", email: "Admin@example.com", password: "123456")
