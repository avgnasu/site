class AddFeaturesToCars < ActiveRecord::Migration
  def change
    add_column :cars, :features, :string
  end
end
