class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|

      t.string  :fuel_type
      t.float   :displacement
      t.integer :year
      t.integer :model_id
      t.integer :make_id
      t.timestamps
    end
  end
end
