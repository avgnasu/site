class AddAvatarRoModels < ActiveRecord::Migration
  def change
    add_column :models, :avatar, :string
    add_column :models, :slug, :string
    add_index :models, :slug
  end
end
